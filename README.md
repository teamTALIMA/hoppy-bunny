Hoppy Bunny
===========
<sup>*teamTALIMA Project #1 &mdash; [Developed LIVE on twitch.tv/teamTALIMA](https://www.twitch.tv/teamtalima)*</sup>

A 2D mobile platform game based on Flappy Bird. :rabbit:

The Android APKs and other platform builds are located under [`/Builds`][builds]. [Download v0.9.3 APK][download_apk]


## Software / Specs

- Unity 5.5.0f3 (Basic)
- Visual Studio, Community 2015 edition
- Programmed in C#


## Needed Contributions

Feel free to submit a merge request!

- *Sound Effects*
    - [x] Bunny Hop
    - [ ] Death from Obstacle Collision (bunny runs/trips into fence or gopher hole)
    - [ ] Background Music - for Main Menu & Game Level (toggle option)
    
- *Scripts / Functions*
    - [x] Obstacle Generation - uniform Fence spacing, randomized Gopher Hole(s) spacing/amount
    - [ ] Android Facebook Sharing (link to PlayStore works, but text doesn't show)
    - [ ] Google Admob - banner loading speed (first ad loads 6-7secs slow)


## Screenshots

<sup>v0.9.1</sup>

<table><tbody><tr><td>
    <img src="/Screenshots/1_v0.9.1.png">
</td><td>
    <img src="/Screenshots/2_v0.9.1.png">
</td><td>
    <img src="/Screenshots/3_v0.9.1.png">
</td><td>
    <img src="/Screenshots/4_v0.9.1.png">
</td></tr></tbody></table>


[builds]: https://gitlab.com/teamTALIMA/hoppy-bunny/tree/master/Builds
[download_apk]: https://gitlab.com/teamTALIMA/hoppy-bunny/blob/master/Builds/hoppy-bunny_v0.9.3.apk
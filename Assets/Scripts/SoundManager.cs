﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManager : MonoBehaviour {

    // Singleton
    public static SoundManager instance = null;

    // Get AudioSource component
    AudioSource _audio;
    AudioSource _audioMusic;
    public bool sfxToggle;
    public bool musicToggle;

    [Header("Game Music")]
    public AudioClip gameMusic;

    [Header("Game Level")]
    public AudioClip addScore;
    public AudioClip bunnyHop;
    public AudioClip bunnyHopTired;
    public AudioClip die;

    [Header("GUI Audio")]
    public AudioClip buttonClick;
    public AudioClip gameOverAppears;
    public AudioClip menuAppears;

    [Header("Scene Change")]
    public AudioClip fadeScene;
    public AudioClip bonusLevelAccess;

    void Awake() {
        // Check if instance already exists, if not set instance to 'this', if instance is not 'this' destory 'this'
        if (instance == null) instance = this;
        else if (instance != this) Destroy(gameObject);

        // Sets this gameObject to not be destroyed when reloading scene
        DontDestroyOnLoad(gameObject);
    }

    void Start() {
        _audio = GetComponent<AudioSource>();
        musicToggle = true;
        sfxToggle = true;

        _audioMusic = this.gameObject.AddComponent<AudioSource>();
        _audioMusic.clip = gameMusic;
        _audioMusic.loop = true;
        _audioMusic.Play();
    }

    /// <summary>
    /// Play the corresponding AudioClip of the string parameter
    /// </summary>
    public void Play(string clip) {
        if (sfxToggle) {
            switch (clip) {
                case "addScore":
                    _audio.PlayOneShot(addScore, 0.8f);
                    //Debug.Log("Playing SFX: addScore");
                    break;

                case "bunnyHop":
                    _audio.PlayOneShot(bunnyHop, 0.3f);
                    //Debug.Log("Playing SFX: bunnyHop");
                    break;

                case "bunnyHopTired":
                    _audio.PlayOneShot(bunnyHopTired, 0.5f);
                    //Debug.Log("Playing SFX: bunnyHopTired");
                    break;

                case "die":
                    _audio.PlayOneShot(die);
                    //Debug.Log("Playing SFX: die");
                    break;

                case "fadeScene":
                    _audio.PlayOneShot(fadeScene, 0.1f);
                    //Debug.Log("Playing SFX: fadeScene");
                    break;

                case "buttonClick":
                    _audio.PlayOneShot(buttonClick, 0.05f);
                    //Debug.Log("Playing SFX: buttonClick");
                    break;

                case "gameOverAppears":
                    //_audio.PlayOneShot(gameOverAppears, 0.1f);
                    //Debug.Log("Playing SFX: gameOverAppears");
                    break;

                case "menuAppears":
                    _audio.PlayOneShot(menuAppears, 0.1f);
                    //Debug.Log("Playing SFX: menuAppears");
                    break;

                case "bonusLevelAccess":
                    _audio.PlayOneShot(bonusLevelAccess, 0.5f);
                    //Debug.Log("Playing SFX: bonusLevelAccess");
                    break;
                default:
                    //Debug.Log("NO SFX: default case");
                    break;
            }
        }
    }

    public void ToggleSFX() {
        sfxToggle = !sfxToggle;
    }

    public void ToggleMusic() {
        if (_audioMusic.isPlaying) {
            musicToggle = false;
            PlayMusic(musicToggle);
        } else {
            musicToggle = true;
            PlayMusic(musicToggle);
        }
    }

    public void PlayMusic(bool play) {
        if (play) _audioMusic.UnPause();
        else _audioMusic.Pause();
    }
}
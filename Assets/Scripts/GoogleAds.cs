﻿using UnityEngine;
using admob;
public class GoogleAds : MonoBehaviour {

    /**Go to https://apps.admob.com/?hl=en&_ga=1.195112297.416167365.1482969341#monetize/reporting:admob/d=1&cc=USD 
    and setup your app information and paste your iDs below first is for banner second is for Interstitial*/
    [Header("Google Admob Settings")]
    public string bannerId = "";
    public string InterstitialId = "";
    public bool testMode = true;

    private Admob ad;

	private void Start () {
         initAdmob();
	}
	
    //bool isAdmobInited = false;
    private void initAdmob()
    { 
        //isAdmobInited = true;
        ad = Admob.Instance();
       
        //initialize your ids with admob
        ad.initAdmob(bannerId, InterstitialId);
       
        //set this to false for release
        ad.setTesting(testMode);
       
        //target male or female
        ad.setGender(AdmobGender.MALE);
        Debug.Log("Admob ad is initialized");
    }

    public void ShowBannerTop() {
        //This shows your ad and positions it at the top center of the screen
        //Admob.Instance().showBannerRelative(AdSize.SmartBanner, AdPosition.TOP_CENTER, 0);
        ad.showBannerRelative(AdSize.SmartBanner, AdPosition.TOP_CENTER, 0);
    }

    public void ShowBannerBottom() {
        //This shows your ad and positions it at the bottom center of the screen
        //Admob.Instance().showBannerRelative(AdSize.SmartBanner, AdPosition.BOTTOM_CENTER, 0);
        ad.showBannerRelative(AdSize.SmartBanner, AdPosition.BOTTOM_CENTER, 0);
    }

    public void RemoveAd() {
        //This removes the banner ad
        Admob.Instance().removeBanner();
    }
}

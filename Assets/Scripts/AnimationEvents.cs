﻿/* REVIEWED & COMPLETED
 * 1/14/17
 */

using UnityEngine;

public class AnimationEvents : MonoBehaviour {

    GameManager _gm;

    void Start() {
        _gm = FindObjectOfType<GameManager>();
    }


    /// <summary>
    /// score-menu-ui animation event - sfx for 'Menu - Score' appearance
    /// </summary>
    public void ScoreMenuSFX() {
        _gm.PlayAudio("menuAppears");
    }

    /// <summary>
    /// game-end-ui animation event - sfx for 'Game End UI -> TXT - Game Over' appearance
    /// </summary>
    public void GameOverSFX() {
        _gm.PlayAudio("gameOverAppears");
    }

    /// <summary>
    /// game-end-ui animation event - sfx for 'Game End UI -> Menu' appearance
    /// </summary>
    public void EndMenuSFX() {
        _gm.PlayAudio("menuAppears");
    }
}
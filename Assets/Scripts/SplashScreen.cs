﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SplashScreen : MonoBehaviour {

    float time = 0;

	// Use this for initialization
	void Start () {
        StartCoroutine(SplashScreenPlay());
	}

    void Update() {
        time += Time.deltaTime;

        Debug.Log(time);
    }

    IEnumerator SplashScreenPlay() {
        // Call Banner Ad here to load faster?

        yield return new WaitForSeconds(1.5f);

        SceneManager.LoadScene(1);
    }
}

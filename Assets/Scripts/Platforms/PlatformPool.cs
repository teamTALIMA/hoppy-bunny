﻿/* REVIEWED & COMPLETED
 * 12/19/16
 */

using UnityEngine;
using System.Collections.Generic;
using System.Linq;

[System.Serializable]
public class PlatformPool {

    // The type of platform our object pool contains
    public PlatformManager.PlatformType type;

    // List of platforms in our object pool
    public List<PlatformComponent> list;

    public PlatformComponent prefab;
    public Bounds bounds;

    Transform _transform;

    // Create a new PlatformPool
    public PlatformPool( Transform manager, PlatformManager.PlatformType platformType, PlatformComponent platformPrefab ){
        // Create gameObject and name for our new object pool
        GameObject go = new GameObject(platformType + "Pool");
        // Assign the PlatformManager as object pool's parent
        go.transform.parent = manager;

        // Assign object pool variables
        this.type = platformType;
        this.list = new List<PlatformComponent>();
        this.prefab = platformPrefab;
        this.bounds = prefab.CalculateBounds();
        this._transform = go.transform;
    }

    // Fill object pool with proper 'count' of objects
    public void Fill(Bounds cameraBounds) {
        // Instantiate first object (platform) for object pool
        PlatformComponent clone = Instantiate();

        // Determine how many objects (platforms) are necessary to fill camera bounds
        Bounds cloneBounds = clone.CalculateBounds();
        int count = Mathf.Clamp(Mathf.CeilToInt(cameraBounds.size.x / cloneBounds.size.x), 0, 12);

        // Instantiate 'count' amount of objects (platforms)
        for (int i = 0; i < count; i++) Instantiate();
    }
    
    // Instantiate and return a PlatformComponent
    PlatformComponent Instantiate() {
        PlatformComponent clone = GameObject.Instantiate<PlatformComponent>(prefab);
        clone.gameObject.SetActive(false);
        clone.transform.parent = _transform;
        list.Add(clone);
        return clone;
    }

    // Check for available objects (platforms)
    public bool CanGetPlatform() {
        return list.Any( platform => !platform.gameObject.activeInHierarchy );
    }

    // Return first available object (platform) in 'list'
    public PlatformComponent GetPlatform() {
        return list.First(p => !p.gameObject.activeInHierarchy);
    }

    // Deactivate 'platform'
    public void ReleasePlatform(PlatformComponent platform) {
        platform.gameObject.SetActive(false);
    }
}
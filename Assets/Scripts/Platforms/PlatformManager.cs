﻿/* REVIEWED & COMPLETED
 * 12/19/16
 */

using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class PlatformManager : MonoBehaviour {

    // Our platform types, used to determine which pool to access
    public enum PlatformType {
        GenericPlatform,
        PipePlatform,
        GopherHolePlatform,
        WhiteFencePlatform
    }

    // Platform prefabs
    public GenericPlatform prefabGenericPlatform;
    public PipePlatform prefabPipePlatform;
    public GopherHolePlatform prefabGopherHolePlatform;
    public WhiteFencePlatform prefabWhiteFencePlatform;

    // Object pool List
    public List<PlatformPool> pools;

    // Platform generation patterns
    public enum GeneratorMode {
        Default,    // Consistent white fences, Random gopher holes
        Original,   // RandomObstacles, with gopher hole always followed by pipe
        OnlyPlatforms,
        OnlyPipes,
        OnlyWhiteFences,
        RandomObstacles
    }

    // Set platform generation pattern
    public GeneratorMode generatorMode = GeneratorMode.Default;

    // The next PlatformType to be activated in-game
    IEnumerator<PlatformType?> curPlatformType;

    // Furthest point of current instantiated platforms
    float _progressionOffset = 0;

    // Camera bounds, needed to determine object pools' Fill() count
    Camera _camera;
    Bounds _cameraBounds {
        get {
            if (_camera == null) return new Bounds(Vector3.zero, Vector3.zero);
            float height = _camera.orthographicSize * 2f;
            float width = _camera.aspect * height;
            return new Bounds(_camera.transform.position, new Vector3(width, height, 100f));
        }
    }

    // Max # of platforms to be generated per frame
    public int maxPlatforms = 100;

    void Awake() {
        // Assign platform prefabs
        if (prefabGenericPlatform == null) prefabGenericPlatform = Resources.Load<GenericPlatform>("Platforms/Platform");
        if (prefabPipePlatform == null) prefabPipePlatform = Resources.Load<PipePlatform>("Platforms/Platform (pipe)");
        if (prefabGopherHolePlatform == null) prefabGopherHolePlatform = Resources.Load<GopherHolePlatform>("Platforms/Platform (gopher hole)");
        if (prefabWhiteFencePlatform == null) prefabWhiteFencePlatform = Resources.Load<WhiteFencePlatform>("Platforms/Platform (white fence)");

        // Create object pools and .Add() them to 'pools' list 
        pools.Add(new PlatformPool(transform, PlatformType.GenericPlatform, prefabGenericPlatform));
        pools.Add(new PlatformPool(transform, PlatformType.PipePlatform, prefabPipePlatform));
        pools.Add(new PlatformPool(transform, PlatformType.GopherHolePlatform, prefabGopherHolePlatform));
        pools.Add(new PlatformPool(transform, PlatformType.WhiteFencePlatform, prefabWhiteFencePlatform));

        // Assign game camera
        _camera = Camera.main;

        // Set progressionOffset for next platform's positioning
        _progressionOffset = (_camera.aspect * _camera.orthographicSize * -1f) + _camera.transform.position.x;

        // Fill object pools
        foreach (PlatformPool pool in pools) {
            pool.Fill(_cameraBounds);
        }

        // Create platform generator, determines next platform to activate based on PlatformType
        curPlatformType = PlatformGenerator();

        // Start coroutine which: cranks generator, releases platforms, waits to activate next platform
        StartCoroutine(PlatformGeneratorCoroutine());
    }

    public IEnumerator<PlatformType?> PlatformGenerator() {
        while (true) {
            // switch on chosen GeneratorMode
            switch (generatorMode) {
                case GeneratorMode.Default: // game's standard generation pattern
                    yield return PlatformType.GopherHolePlatform;
                    yield return PlatformType.WhiteFencePlatform;
                    break;
                case GeneratorMode.Original: // game's original generation pattern
                    int randOri = Random.Range(0, 2);
                    if(randOri == 0) {
                        yield return PlatformType.PipePlatform;
                    } else if (randOri == 1) {
                        yield return PlatformType.GopherHolePlatform;
                        yield return PlatformType.PipePlatform;
                    }
                    break;
                case GeneratorMode.OnlyPlatforms: // game's starting generation pattern
                    yield return PlatformType.GenericPlatform;
                    break;
                case GeneratorMode.OnlyPipes:
                    yield return PlatformType.PipePlatform;
                    break;
                case GeneratorMode.OnlyWhiteFences:
                    yield return PlatformType.WhiteFencePlatform;
                    break;
                case GeneratorMode.RandomObstacles:
                    yield return Random.Range(0, 2) == 0 ? PlatformType.PipePlatform : PlatformType.GopherHolePlatform;
                    break;
            }
            yield return null;
        }
    }

    IEnumerator PlatformGeneratorCoroutine() {
        while (true) {
            ReleasePlatforms();
            bool waited = false;
            
            for(int i = 0; i < maxPlatforms; i++) {
                // Crank generator
                curPlatformType.MoveNext();
                
                // If no PlatformPool in 'pools' has 'type' of required PlatformType, then continue
                if (!pools.Any(p => p.type == curPlatformType.Current)) continue;

                // Assign first PlatformPool with 'type' of required PlatformType
                PlatformPool pool = pools.First(p => p.type == curPlatformType.Current);

                // If pool has no available platforms, then wait for a platform to be released
                if (!pool.CanGetPlatform()) {
                    yield return new WaitUntil(() => {
                        ReleasePlatforms();
                        return pool.CanGetPlatform();
                    });
                    waited = true;
                }

                // Assign first available platform
                PlatformComponent platform = pool.GetPlatform();

                // Activate platform
                platform.transform.eulerAngles = Vector2.zero;
                platform.transform.localScale = Vector2.one;
                platform.transform.position = new Vector2(_progressionOffset + pool.bounds.extents.x - pool.bounds.center.x, _cameraBounds.min.y + platform.height);
                platform.gameObject.SetActive(true);
                platform.SendMessage("PlatformSetup", SendMessageOptions.DontRequireReceiver);

                // Update _progressionOffset for next platform's position
                _progressionOffset += pool.bounds.size.x;

                if (waited) break;
            }
            if (!waited)
                yield return new WaitForEndOfFrame();
        }
    }

    // Loop through object pools to release/deactivate used platforms
    void ReleasePlatforms() {
        Bounds camBounds = _cameraBounds;
        foreach(var pool in pools) {
            foreach(var platform in pool.list) {
                // If platform is active, is left of camera and is not intersecting with camera, then release/deactivate
                if (platform.gameObject.activeInHierarchy && platform.transform.position.x < _camera.transform.position.x && !camBounds.Intersects(platform.CalculateBounds()))
                    pool.ReleasePlatform(platform);
            }
        }
    }
}
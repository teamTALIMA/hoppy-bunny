﻿/* REVIEWED & COMPLETED
 * 1/5/16
 */

using UnityEngine;

public class GopherHolePlatform : PlatformComponent {

    // Set obstacles' spawn bounds
    float minX = -3.5f;
    float maxX = 3.5f;
    float _offset = 0;

    void PlatformSetup() {
        int childrenCount = transform.childCount;
        int rand = Random.Range(0, childrenCount);

        // width (+ offset) and extent of gopher hole sprite:
        float width = transform.GetChild(0).GetComponent<SpriteRenderer>().bounds.size.x + _offset;
        float extent = width / 2f;

        // initial x and y values:
        float x = minX; // increments as gopher holes are placed in the foreach loop.
        float y = transform.GetChild(0).localPosition.y;

        // remaining space:
        float remaining = Mathf.Abs(minX - maxX); // de-increments as gopher holes are placed in the foreach loop.

        // handle edge case where you can't fit `rand` number of the gopher holes in the available space:
        if (width * (float)(rand + 1) > remaining) {
            // uncomment if you want to be notified of this condition:
            //Debug.LogFormat("edge case warning: {0} * {1} > {2}", width, rand + 1, remaining);

            // update width and extent values:
            width = remaining / (float)(rand + 1);
            extent = width / 2f;
        }

        // iterate over all of the gopher holes:
        for (int childIndex = 0; childIndex < childrenCount; childIndex++) {
            Transform child = transform.GetChild(childIndex);

            // activate up to `rand` gopher holes, deactivate the rest:
            bool isActive = childIndex <= rand;
            child.gameObject.SetActive(isActive);

            // skip to next in foreach loop if isActive is false:
            if (!isActive) continue;

            float fraction = (float)((rand - childIndex) + 1);

            // "thinking of others" or "fair" portion algorithm: - spread evenly
            float maxRandX = (remaining / fraction) - width;
            // "thinking of myself" or "greedy" portion algorithm: - groups right
            //float maxRandX = remaining - (width * (fraction - 1f));

            // random value based on the remaining space, between 0 and maxRandX (but keep maxRandX from being < 0.0f):
            float randX = Random.Range(0f, Mathf.Max(0f, maxRandX));

            // eat away at remaining space:
            remaining -= randX + width;

            // grow x value:
            x += randX + extent;

            child.localPosition = new Vector2(x, y);

            // grow x value again:
            x += extent;
        }
    }

    // debugging available space in the editor:
    void OnDrawGizmosSelected() {
        Gizmos.matrix = transform.localToWorldMatrix;
        Gizmos.color = Color.magenta;
        Gizmos.DrawWireCube(new Vector3(Mathf.Lerp(minX, maxX, 0.5f), 0, 0), new Vector3(Mathf.Abs(minX - maxX), 3, 3));
    }
}





//    // Platform w/ GopherHole obstacle

//    float minX = -3.5f;
//    float maxX = 3.5f;
//    float obstacleY = .5f;

//    float[] _lastPos = new float[2];
//    float _bound;
//    float _offset = 0.1f;

//    // Triggered through SendMessage by PlatformManager in PlatformGeneratorCoroutine
//    void PlatformSetup() {
//        // Determine # of obstacles for the platform
//        int rand = Random.Range(0, transform.childCount);

//        // Determine width of obstacles
//        float _bound = transform.GetChild(0).GetComponent<SpriteRenderer>().bounds.size.x;

//        // Iterate through obstacles
//        for (int t = 0; t < transform.childCount; t++) {
//            Transform child = transform.GetChild(t);

//            // Activate 'rand' amount of obstacles
//            child.gameObject.SetActive(t <= 2);

//            if (child.gameObject.activeSelf) {
//                // Set localPosition
//                child.localPosition = new Vector2(Random.Range(minX, maxX), obstacleY);

//                if(t != 0) {

//                    // If we are overlapping with another obstacle
//                    if (_lastPos[0] - _bound < child.localPosition.x && child.localPosition.x < _lastPos[0] + _bound) {
//                        // Overlapping to the left
//                        if (child.localPosition.x <= _lastPos[0]) {
//                            float overlap = _bound - (_lastPos[0] - child.localPosition.x);
//                            child.localPosition = new Vector2(child.localPosition.x - overlap, obstacleY);
//                        }
//                        // Overlapping to the right
//                        else {
//                            float overlap = _bound - (child.localPosition.x - _lastPos[0]);
//                            child.localPosition = new Vector2(child.localPosition.x + overlap, obstacleY);
//                        }
//                    }
//                }

//                if (t < 2) _lastPos[t] = child.localPosition.x;


//                //if (_lastPos != null) {
//                //    for (int i = 0; i < _lastPos.Length; i++) {
//                //        if (_lastPos[i] - _bound < child.localPosition.x && child.localPosition.x < _lastPos[i] + _bound) {
//                //            // If child.lP is less than _lastPos, then it overlaps on the left so move it's 'x' position down
//                //            if (child.localPosition.x < _lastPos[i]) {
//                //                float overlap = _bound - (_lastPos[i] - child.localPosition.x);
//                //                child.localPosition = new Vector2(child.localPosition.x - overlap, obstacleY);
//                //                Debug.Log("CONFLICT lesser: new position = " + child.localPosition.x);

//                //                // If child.lP overlaps second obstacle, then move obstacle over to left
//                //                if (i == 0 && _lastPos[1] - _bound < child.localPosition.x && child.localPosition.x < _lastPos[1] + _bound) {
//                //                    Transform obst = transform.GetChild(1);
//                //                    float obstOverlap = _bound - (child.localPosition.x - obst.localPosition.x);
//                //                    obst.localPosition = new Vector2(obst.localPosition.x - obstOverlap, obstacleY);
//                //                    Debug.Log("SECOND CONFLICT lesser: obst new position = " + obst.localPosition.x);
//                //                } else if (_lastPos[0] - _bound < child.localPosition.x && child.localPosition.x < _lastPos[0] + _bound) {
//                //                    Transform obst = transform.GetChild(2);
//                //                    float obstOverlap = _bound - (child.localPosition.x - obst.localPosition.x);
//                //                    obst.localPosition = new Vector2(obst.localPosition.x - obstOverlap, obstacleY);
//                //                    Debug.Log("SECOND CONFLICT lesser: obst new position = " + obst.localPosition.x);
//                //                }
//                //                // Else, child.lP is greater than _lastPos, and it overlaps on the right so move it's 'x' position up
//                //            } else {
//                //                float overlap = _bound - (child.localPosition.x - _lastPos[i]);
//                //                child.localPosition = new Vector2(child.localPosition.x + overlap, obstacleY);
//                //                Debug.Log("CONFLICT greater: new position = " + child.localPosition.x);

//                //                // If child.lP overlaps second obstacle, then move obstacle over to right
//                //                if (i == 0 && _lastPos[1] - _bound < child.localPosition.x && child.localPosition.x < _lastPos[1] + _bound) {
//                //                    Transform obst = transform.GetChild(1);
//                //                    float obstOverlap = _bound - (obst.localPosition.x - child.localPosition.x);
//                //                    obst.localPosition = new Vector2(obst.localPosition.x + obstOverlap, obstacleY);
//                //                    Debug.Log("SECOND CONFLICT greater: obst new position = " + obst.localPosition.x);
//                //                } else if (_lastPos[0] - _bound < child.localPosition.x && child.localPosition.x < _lastPos[0] + _bound) {
//                //                    Transform obst = transform.GetChild(2);
//                //                    float obstOverlap = _bound - (obst.localPosition.x - child.localPosition.x);
//                //                    obst.localPosition = new Vector2(obst.localPosition.x + obstOverlap, obstacleY);
//                //                    Debug.Log("SECOND CONFLICT greater: obst new position = " + obst.localPosition.x);
//                //                }
//                //            }
//                //        }
//                //    }
//                //}
//            }
//        }
//    }
//}










//public class GopherHolePlatform : PlatformComponent {
//    // Platform w/ GopherHole obstacle

//    float minX = -3.5f;
//    float maxX = 3.5f;
//    float obstacleY = .5f;

//    float[] _lastPos = new float[2];
//    float _bound;
//    float _offset = 0.1f;

//    // Triggered through SendMessage by PlatformManager in PlatformGeneratorCoroutine
//    void PlatformSetup() {
//        // Determine # of obstacles for the platform
//        int rand = Random.Range(0, transform.childCount);

//        // Determine width of obstacles
//        float _bound = transform.GetChild(0).GetComponent<SpriteRenderer>().bounds.size.x;
//        Debug.Log("NEW PLATFORM: bound = " + _bound);

//        // For obstacle (transform child) {
//        for(int t = 0; t < transform.childCount; t++) {
//            Transform child = transform.GetChild(t);

//            // Activate 'rand' amount of obstacles and set localPosition
//            child.gameObject.SetActive(child.GetSiblingIndex() <= 2);
//            if (child.gameObject.activeSelf) {
//                child.localPosition = new Vector2(Random.Range(minX, maxX), obstacleY);
//                Debug.Log("localPosition of child(" + child.GetSiblingIndex() + "): " + child.localPosition.x);

//                // Ensure minimal overlapping,
//                // If child.lP is greater than (_lastPosition - bounds) AND lesser than (_lastPosition + bounds), then...
//                // it overlaps with the previous obstacle
//                if (_lastPos != null) {
//                    for (int i = 0; i < _lastPos.Length; i++) {
//                        if (_lastPos[i] - _bound < child.localPosition.x && child.localPosition.x < _lastPos[i] + _bound) {
//                            // If child.lP is less than _lastPos, then it overlaps on the left so move it's 'x' position down
//                            if (child.localPosition.x < _lastPos[i]) {
//                                float overlap = _bound - (_lastPos[i] - child.localPosition.x);
//                                child.localPosition = new Vector2(child.localPosition.x - overlap, obstacleY);
//                                Debug.Log("CONFLICT lesser: new position = " + child.localPosition.x);

//                                // If child.lP overlaps second obstacle, then move obstacle over to left
//                                if (i == 0 && _lastPos[1] - _bound < child.localPosition.x && child.localPosition.x < _lastPos[1] + _bound) {
//                                    Transform obst = transform.GetChild(1);
//                                    float obstOverlap = _bound - (child.localPosition.x - obst.localPosition.x);
//                                    obst.localPosition = new Vector2(obst.localPosition.x - obstOverlap, obstacleY);
//                                    Debug.Log("SECOND CONFLICT lesser: obst new position = " + obst.localPosition.x);
//                                } else if (_lastPos[0] - _bound < child.localPosition.x && child.localPosition.x < _lastPos[0] + _bound) {
//                                    Transform obst = transform.GetChild(2);
//                                    float obstOverlap = _bound - (child.localPosition.x - obst.localPosition.x);
//                                    obst.localPosition = new Vector2(obst.localPosition.x - obstOverlap, obstacleY);
//                                    Debug.Log("SECOND CONFLICT lesser: obst new position = " + obst.localPosition.x);
//                                }
//                                // Else, child.lP is greater than _lastPos, and it overlaps on the right so move it's 'x' position up
//                            } else {
//                                float overlap = _bound - (child.localPosition.x - _lastPos[i]);
//                                child.localPosition = new Vector2(child.localPosition.x + overlap, obstacleY);
//                                Debug.Log("CONFLICT greater: new position = " + child.localPosition.x);

//                                // If child.lP overlaps second obstacle, then move obstacle over to right
//                                if (i == 0 && _lastPos[1] - _bound < child.localPosition.x && child.localPosition.x < _lastPos[1] + _bound) {
//                                    Transform obst = transform.GetChild(1);
//                                    float obstOverlap = _bound - (obst.localPosition.x - child.localPosition.x);
//                                    obst.localPosition = new Vector2(obst.localPosition.x + obstOverlap, obstacleY);
//                                    Debug.Log("SECOND CONFLICT greater: obst new position = " + obst.localPosition.x);
//                                } else if (_lastPos[0] - _bound < child.localPosition.x && child.localPosition.x < _lastPos[0] + _bound) {
//                                    Transform obst = transform.GetChild(2);
//                                    float obstOverlap = _bound - (obst.localPosition.x - child.localPosition.x);
//                                    obst.localPosition = new Vector2(obst.localPosition.x + obstOverlap, obstacleY);
//                                    Debug.Log("SECOND CONFLICT greater: obst new position = " + obst.localPosition.x);
//                                }
//                            }
//                        }
//                    }
//                }

//                if(t < 2) {
//                    _lastPos[t] = child.localPosition.x;
//                    Debug.Log("_lastPos[" + t + "]: " + _lastPos[t]);
//                }
//            }
//        }
//    }

//    public void NewObstPos(Vector3 newObstPos, float overlappingObst, bool right = true) {
//        if(right) {
//            float obstOverlap = _bound - (newObstPos.x - overlappingObst);
//            newObstPos = new Vector2(newObstPos.x + obstOverlap - _offset, obstacleY);
//        } else {
//            float obstOverlap = _bound - (overlappingObst - newObstPos.x);
//            newObstPos = new Vector2(newObstPos.x - obstOverlap + _offset, obstacleY);
//        }
//    }
//}





//float x = minX;
//// width of the gopher hole sprite:
//float width = 1.2f;
//// remaining space:
//float remaining = Mathf.Abs(minX - maxX);
//float y = 0.6f;

//foreach (Transform child in transform) {
//    int childIndex = child.GetSiblingIndex();
//    bool isActive = childIndex <= rand;
//    child.gameObject.SetActive(isActive);
//    if (!isActive) continue;
//    float fraction = (float)((rand - childIndex) + 1);
//    // random value based on what would be a "fair" portion of the remaining space:
//    float randX = Random.Range(0f, remaining / fraction);
//    remaining -= randX + width;
//    x += randX;
//    child.localPosition = new Vector2(x, y);
//}





//public class GopherHolePlatform : PlatformComponent {
//    // Platform w/ GopherHole obstacle

//    float minX = -3.5f;
//    float maxX = 3.5f;
//    float obstacleY = .5f;

//    float[] _lastPos = new float[2];
//    float _bound;
//    float _offset = 0.1f;

//    // Triggered through SendMessage by PlatformManager in PlatformGeneratorCoroutine
//    void PlatformSetup() {
//        // Determine # of obstacles for the platform
//        int rand = Random.Range(0, transform.childCount);

//        // Determine width of obstacles
//        float _bound = transform.GetChild(0).GetComponent<SpriteRenderer>().bounds.size.x;

//        // Iterate through obstacles
//        for (int t = 0; t < transform.childCount; t++) {
//            Transform child = transform.GetChild(t);

//            // Activate 'rand' amount of obstacles
//            child.gameObject.SetActive(t <= 2);

//            if (child.gameObject.activeSelf) {
//                // Set localPosition
//                child.localPosition = new Vector2(Random.Range(minX, maxX), obstacleY);

//                if (t != 0) {
//                    // If we are overlapping with another obstacle
//                    if (_lastPos[0] - _bound < child.localPosition.x && child.localPosition.x < _lastPos[0] + _bound) {
//                        // Overlapping to the left
//                        if (child.localPosition.x <= _lastPos[0]) {
//                            float overlap = _bound - (_lastPos[0] - child.localPosition.x);
//                            child.localPosition = new Vector2(child.localPosition.x - overlap, obstacleY);
//                        }
//                        // Overlapping to the right
//                        else if (child.localPosition.x > _lastPos[0]) {
//                            float overlap = _bound - (child.localPosition.x - _lastPos[0]);
//                            child.localPosition = new Vector2(child.localPosition.x + overlap, obstacleY);
//                        }
//                    }
//                }

//                if (t < 2) _lastPos[t] = child.localPosition.x;
//            }
//        }
//    }
//}